package service

import (
	"github.com/sugengwin/banking/domain"
	"github.com/sugengwin/banking/errs"
	"github.com/sugengwin/banking/repository"
)

type CustomerService interface {
	GetAllCustomer() ([]domain.Customer, *errs.AppError)
	GetAllCustomerWithStatus(status string) ([]domain.Customer, *errs.AppError)
	GetCustomer(id string) (*domain.Customer, *errs.AppError)
}

type DefaultCustomerService struct {
	repo repository.ICustomerRepository
}

func (s DefaultCustomerService) GetAllCustomer() ([]domain.Customer, *errs.AppError) {
	return s.repo.FindAll()
}

func (s DefaultCustomerService) GetAllCustomerWithStatus(status string) ([]domain.Customer, *errs.AppError) {
	return s.repo.FindAllWithStatus(status)
}

func (s DefaultCustomerService) GetCustomer(id string) (*domain.Customer, *errs.AppError) {
	return s.repo.ById(id)
}

func NewCustomerService(repository repository.ICustomerRepository) DefaultCustomerService {
	return DefaultCustomerService{repository}
}
