package main

import (
	"github.com/sugengwin/banking/app"
	"github.com/sugengwin/banking/logger"
)

func main() {
	//log.Println("Starting our application...")
	logger.Info("Starting our application")
	app.Start()
}
