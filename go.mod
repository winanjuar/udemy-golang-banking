module github.com/sugengwin/banking

go 1.20

require (
	github.com/go-sql-driver/mysql v1.7.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	go.uber.org/zap v1.24.0 // indirect
)
