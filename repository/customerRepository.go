package repository

import (
	"github.com/sugengwin/banking/domain"
	"github.com/sugengwin/banking/errs"
)

type ICustomerRepository interface {
	FindAll() ([]domain.Customer, *errs.AppError)
	FindAllWithStatus(status string) ([]domain.Customer, *errs.AppError)
	ById(id string) (*domain.Customer, *errs.AppError)
}
