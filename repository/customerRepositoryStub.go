package repository

import "github.com/sugengwin/banking/domain"

type CustomerRepositoryStub struct {
	customers []domain.Customer
}

func (s CustomerRepositoryStub) FindAll() ([]domain.Customer, error) {
	return s.customers, nil
}

func NewCustomerRepositoryStub() CustomerRepositoryStub {
	customers := []domain.Customer{
		{Id: "1001", Name: "Sugeng", City: "Cirebon", Zipcode: "45188", DateOfBirth: "1988-04-07", Status: "1"},
		{Id: "1002", Name: "Noura", City: "Jakarta", Zipcode: "12710", DateOfBirth: "2021-09-10", Status: "1"},
	}
	return CustomerRepositoryStub{customers}
}
