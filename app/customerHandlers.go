package app

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sugengwin/banking/domain"
	"github.com/sugengwin/banking/errs"
	"github.com/sugengwin/banking/service"
	"net/http"
	"strings"
	"time"
)

type CustomerHandlers struct {
	service service.CustomerService
}

func (ch *CustomerHandlers) getAllCustomers(w http.ResponseWriter, r *http.Request) {
	var customers []domain.Customer
	var err *errs.AppError
	statusQueryParam := r.URL.Query().Get("status")
	if statusQueryParam != "" {
		var status string
		if statusQueryParam == "active" {
			status = "1"
		} else {
			status = "0"
		}
		customers, err = ch.service.GetAllCustomerWithStatus(status)
	} else {
		customers, err = ch.service.GetAllCustomer()
	}

	if err != nil {
		writeResponse(w, err.Code, err.AsMessage())
	} else {
		writeResponse(w, http.StatusOK, customers)
	}
}

func (ch *CustomerHandlers) getCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["customer_id"]

	customer, err := ch.service.GetCustomer(id)
	if err != nil {
		writeResponse(w, err.Code, err.AsMessage())
	} else {
		writeResponse(w, http.StatusOK, customer)
	}

}

func getCurrentTime(w http.ResponseWriter, r *http.Request) {
	response := make(map[string]string)
	tz := r.URL.Query().Get("tz")

	if tz != "" {
		timezones := strings.Split(tz, ",")
		for _, timezone := range timezones {
			loc, err := time.LoadLocation(timezone)
			if err != nil {
				w.WriteHeader(http.StatusNotFound)
				_, _ = fmt.Fprintf(w, "Timezone %s not found", timezone)
				return
			}
			response[timezone] = time.Now().In(loc).String()
		}
	} else {
		response["current_time"] = time.Now().UTC().String()
	}

	w.Header().Add("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(response)
}

func writeResponse(w http.ResponseWriter, code int, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	if err := json.NewEncoder(w).Encode(data); err != nil {
		panic(err)
	}
}
