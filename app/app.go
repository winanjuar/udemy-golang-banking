package app

import (
	"github.com/gorilla/mux"
	"github.com/sugengwin/banking/repository"
	"github.com/sugengwin/banking/service"
	"log"
	"net/http"
)

func Start() {
	router := mux.NewRouter()

	// wiring
	//ch := CustomerHandlers{service.NewCustomerService(repository.NewCustomerRepositoryStub())}
	ch := CustomerHandlers{service.NewCustomerService(repository.NewCustomerRepositoryDB())}

	// define routes
	router.HandleFunc("/customers", ch.getAllCustomers).Methods(http.MethodGet)
	router.HandleFunc("/customers/{customer_id:[0-9]+}", ch.getCustomer).Methods(http.MethodGet)
	router.HandleFunc("/api/time", getCurrentTime).Methods(http.MethodGet)

	// starting server
	log.Fatal(http.ListenAndServe("localhost:8080", router))
}
